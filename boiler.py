#! /usr/bin/python

"""Monitor the electrical boiler with a Raspberry Pi."""

import argparse
import logging
import time
from datetime import datetime
import RPi.GPIO as GPIO
from max31855 import MAX31855, MAX31855Error

LOG_FILENAME = "/home/pi/boiler/boiler.log"
LOG_LEVEL = logging.INFO  # Could be e.g. "DEBUG" or "WARNING"
SAMPLETIME = 30 # sec

# Define and parse command line arguments
parser = argparse.ArgumentParser(description="Monitor electrical boiler")
parser.add_argument("-l", "--log", help="file to write log to (default '" + LOG_FILENAME + "')")

# If the log file is specified on the command line then override the default
args = parser.parse_args()
if args.log:
	LOG_FILENAME = args.log

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger(__name__)
# Set the log level to LOG_LEVEL
logger.setLevel(LOG_LEVEL)
# Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
handler = logging.FileHandler(LOG_FILENAME)
# Format each log message like this
formatter = logging.Formatter('%(asctime)s %(message)s', 
			      "%Y-%m-%d %H:%M:%S")
# Attach the formatter to the handler
handler.setFormatter(formatter)
# Attach the handler to the logger
logger.addHandler(handler)


def ledbeat():
    """Blimp the green LED on the RPi."""
    LEDIO = 16
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(LEDIO, GPIO.OUT)
    GPIO.output(LEDIO, GPIO.LOW)# on
    time.sleep(0.1)
    GPIO.output(LEDIO, GPIO.HIGH)# off


# the main sensor reading loop
while True:
    ledbeat()
    try:
	    temp1 = "%.02f" % MAX31855(24, 23, 21, 'c', GPIO.BOARD).get()
    except  MAX31855Error:
	    temp1 = '-'
    try:
	    temp2 = "%.02f" % MAX31855(26, 23, 21, 'c', GPIO.BOARD).get()
    except  MAX31855Error:
	    temp2 = "-"
    s = "%s %s" % (temp1, temp2)
    logger.info(s)
    # delay between stream posts
    time.sleep(SAMPLETIME)

fid.close()
